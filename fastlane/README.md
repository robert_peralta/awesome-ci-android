fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android development_debug
```
fastlane android development_debug
```
Build Development Debug App
### android development_release
```
fastlane android development_release
```
Build Development Release App
### android demo_debug
```
fastlane android demo_debug
```
Build Demo Debug App
### android demo_release
```
fastlane android demo_release
```
Build Demo Release App
### android staging_debug
```
fastlane android staging_debug
```
Build Staging Debug App
### android staging_release
```
fastlane android staging_release
```
Build Staging Release App
### android qa_debug
```
fastlane android qa_debug
```
Build QA Debug App
### android qa_release
```
fastlane android qa_release
```
Build QA Release App
### android production_debug
```
fastlane android production_debug
```
Build Production Debug App
### android production_release
```
fastlane android production_release
```
Build Production Release App
### android test
```
fastlane android test
```
Runs all the tests
### android beta
```
fastlane android beta
```
Submit a new Beta Build to Crashlytics Beta
### android deploy
```
fastlane android deploy
```
Deploy a new version to the Google Play

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
